function calcularMulta(){
    var inValorVelocidadePermitida = document.getElementById("inValorVelocidadePermitida")
    var inValorVelocidadeCondutor = document.getElementById("inValorVelocidadeCondutor")
    
    var outMulta = document.getElementById("outMulta")

    var VelocidadePermitida = Number(inValorVelocidadePermitida.value)
    var VelocidadeCondutor = Number(inValorVelocidadeCondutor.value)

    if(inValorVelocidadePermitida.value == "" || isNaN(VelocidadePermitida)){
        alert("Velocidade Permitida não informada")
        inValorVelocidadePermitida.focus()
        return
    } else if(inValorVelocidadeCondutor.value == "" || isNaN(VelocidadeCondutor)){
        alert("Velocidade do Condutor não informada")
        inValorVelocidadeCondutor.focus()
        return
    }

    /** 
    *Se a velocidade for inferior ou igual à permitida, exiba “Sem
    *Multa”. Se a velocidade for de até 20% maior que a permitida, 
    *exiba “Multa Leve”. E, se a velocidade for superior a 20% da 
    *velocidade permitida, exiba “Multa Grave”
    */
   
    if(VelocidadeCondutor <= VelocidadePermitida){
        outMulta.textContent = "Sem Multa"
    } else if(VelocidadeCondutor <= VelocidadePermitida*1.2 ){
        outMulta.textContent = "Multa leve"
    } else if(VelocidadeCondutor > VelocidadePermitida*1.2 ){
        outMulta.textContent = "Multa grave"
    } 

}

var btExibir = document.getElementById("btExibir")
btExibir.addEventListener("click",calcularMulta)