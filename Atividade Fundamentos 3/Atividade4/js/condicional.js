function calcularFuso(){
    var inValor = document.getElementById("inValor")
    var outTempo = document.getElementById("outTempo")
    var outTroco = document.getElementById("outTroco")

    var numero = Number(inValor.value)

    if(inValor.value == "" || isNaN(numero)){
        alert("Valor não informado")
        inValor.focus()
        return
    }

    var teste = {1.0: 30 , 1.75: 60, 3.00: 120 }
    var tempo = "Tempo: "
    var troco = "Troco R$: "
    if(numero >= 3){
        outTroco.textContent = troco + (numero - 3).toFixed(2)
        outTempo.textContent = tempo+"120 min"
    } else if(numero >= 1.75){
        outTroco.textContent = troco + (numero - 1.75).toFixed(2)
        outTempo.textContent = tempo+"60 min"
    } else if(numero >= 1.00){
        outTroco.textContent = troco + (numero - 1.00).toFixed(2)
        outTempo.textContent = tempo+"30 min"
    } else if(numero < 1){
        alert("Valor insuficiente")
        outTroco.textContent = ""
        outTempo.textContent = ""
    }

}

var btExibir = document.getElementById("btExibir")
btExibir.addEventListener("click",calcularFuso)