document.getElementById("radioTrue").onclick = function() {
    document.getElementById("selectConvenio").className = "exibe"
}
document.getElementById("radioFalse").onclick = function() {
    document.getElementById("selectConvenio").className = "oculta"
}




function calcularPreco(){
    var inValor = document.getElementById("inValor")
    
    var outDesconto = document.getElementById("outDesconto")
    var outPagar = document.getElementById("outPagar")
    
    var valor = Number(inValor.value)

    if(inValor.value == "" || isNaN(valor)|| valor<0){
        alert("Informe valor")
        inValor.focus()
        return
    }

    desconto = calcularDesconto(valor)

    outDesconto.textContent = `Desconto R$: ${desconto.toFixed(2)}`
    outPagar.textContent = `A Pagar R$: ${(valor - desconto).toFixed(2)}`

    
}

function calcularDesconto(valor){
    var inConvenio = document.getElementById("selectConvenio")
    if(inConvenio.className == "exibe"){
        var inConvenio = document.getElementById("convenio")
        inConvenio.value == 1 ? desconto = valor * 0.2 : desconto = valor * 0.5
    } else{
        desconto = valor * 0.1
    }
    return desconto
}



var btExibir = document.getElementById("btExibir")
btExibir.addEventListener("click",calcularPreco)