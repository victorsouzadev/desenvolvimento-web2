function repeticao(){
    var inValor = document.getElementById("inValor")
    var outNumPerfeito = document.getElementById("outNumPerfeito")

    var numero = Number(inValor.value)

    //Validação da entrada
    if(inValor.value == "" || isNaN(numero)){
        alert("Número não informado")
        inValor.focus()
        return
    }
    divisores = []
    out= ""
    soma = 0
    for(let i = 0; i<numero; i++){
        div = numero/i
        if(Number.isInteger(div)){
            divisores.push(i) 
            soma += i
        }
    }
   
    
    if(soma==numero){
        out += ` (Soma:${soma})\n${numero} É um número perfeito`
    } else{
        out += ` (Soma:${soma})\n${numero} Não é um número perfeito`
    }
    

    //saída
    outNumPerfeito.textContent = `Divisores do ${numero}: ${divisores} ${out}`
}

var btExibir = document.getElementById("btExibir")
btExibir.addEventListener("click",repeticao)