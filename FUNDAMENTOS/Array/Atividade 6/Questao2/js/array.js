var crianca = []

function addCrianca(){
    var inNome = document.getElementById("inNome")
    var inIdade = document.getElementById("inIdade")
    nome = inNome.value
    idade = Number(inIdade.value)
   
    //Validação
    if(idade <= 0 || isNaN(idade)){
        alert("Informe uma idade válida!")
        inIdade.focus()
        return
    }
    if( nome.trim() ==""){
        alert("Informe um nome válida!")
        inNome.focus()
        return
    }
    
    crianca.push({nome:nome,idade:idade})
}

function listarAlunos(){
    var out = document.getElementById("out")
    out.textContent = ''
    var lista = crianca.map(crianca => `${crianca.nome} - ${crianca.idade}\n`)
    for(i in lista){
        out.textContent += lista[i]
    }
    
}
function resumirPorIdade(){
    var out = document.getElementById("out")
    out.textContent =""
    var idade = crianca.map(crianca => crianca.idade)
    idadeUnique = unique(idade)
    for( i in idadeUnique){
        var filtro = crianca => crianca.idade == idadeUnique[i]
        var name = crianca.filter(filtro).map(crianca=>`${crianca.nome}`)
        var idadeCrianca = idadeUnique[i]
        var quantidade = count(idade,idadeCrianca)
        
        out.textContent += `${idadeCrianca} ano(s): ${quantidade} crianca(s) - ${(parseFloat(quantidade)/parseFloat(idade.length) * 100.00).toFixed(2)}%\n(${name})\n`
    }

    
    
    
    //out.textContent = 
}

var btResumIdade = document.getElementById("btResumIdade")
btResumIdade.addEventListener("click",resumirPorIdade)

var btListAll = document.getElementById("btListAll")
btListAll.addEventListener("click",listarAlunos)

var btAdd = document.getElementById("btAdd")
btAdd.addEventListener("click",addCrianca)

function unique(array){
    array.sort()
   
    arrayUnique = []
    arrayUnique.push(array[0])
    for(i in array){
        if(array[i]!=arrayUnique[arrayUnique.length -1]){
            arrayUnique.push(array[i])
        }
    }
    return arrayUnique
}

function count(array,numero){
    contador = 0
    for(i in array){
        if(array[i]==numero){
            contador++
        }
    }
    return contador
}