function repeticao(){
    var inValor = document.getElementById("inValor")
    var inValorAno = document.getElementById("inValorAno")
    var outChinchilas = document.getElementById("outChinchilas")

    var numero = Number(inValor.value)
    var ano = Number(inValorAno.value)
    //Validação da entrada
    if(inValor.value == "" || isNaN(numero)){
        alert("Número não informado")
        inValor.focus()
        return
    }
    if(inValorAno.value == "" || isNaN(ano)){
        alert("Número não informado")
        inValorAno.focus()
        return
    }
    out =""
    if(numero>=2){
        for(let i = 1; i<=ano;i++){
            out+= `${i}º Ano: ${numero} Chinchilas\n`
            numero = numero*3   
        }
        //saída
        outChinchilas.textContent = out
    } else{
        //saída
        alert("Número de Chinchilas deve ser maior ou igual a 2")
    }
    
    
    
}

var btExibir = document.getElementById("btExibir")
btExibir.addEventListener("click",repeticao)