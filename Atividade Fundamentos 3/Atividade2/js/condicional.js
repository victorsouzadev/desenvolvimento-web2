function parImpar(){
    var inValor = document.getElementById("inValor")
    var outParImpar = document.getElementById("outParImpar")

    var numero = Number(inValor.value)
    var resposta = ""
    
    if(inValor.value == "" || isNaN(numero)){
        alert("Número não informado")
        inValor.focus()
        return
    }

    if(numero != 0){
        
        if(numero % 2 == 0){
            
            resposta = "Par"
        } else {
            resposta = "Impar"
        }
    
    } else {
        resposta = "Indefinido"
    }
    

outParImpar.textContent = "Resposta: "+numero+" é "+resposta
    
}

var btExibir = document.getElementById("btExibir")
btExibir.addEventListener("click",parImpar)