function calcularFuso(){
    var inValorA = document.getElementById("inValorA")
    var inValorB = document.getElementById("inValorB")
    var inValorC = document.getElementById("inValorC")
    var outSituacao = document.getElementById("outSituacao")
    var outTipo = document.getElementById("outTipo")

    var ValorA = Number(inValorA.value)
    var ValorB = Number(inValorB.value)
    var ValorC = Number(inValorC.value)

    if(inValorA.value == "" || isNaN(ValorA)){
        alert("Número não informado")
        inValorA.focus()
        return
    }else if(inValorB.value == "" || isNaN(ValorB)){
        alert("Número não informado")
        inValorB.focus()
        return
    }else if(inValorC.value == "" || isNaN(ValorC)){
        alert("Número não informado")
        inValorC.focus()
        return
    }
    
    var triangulo = true;
    if(ValorA > ValorB + ValorC){
        triangulo = false; 
    } else if(ValorB > ValorA + ValorC){
        triangulo = false; 
    } else if(ValorC > ValorA + ValorB){
        triangulo = false; 
    }

    
    if(triangulo){

        outSituacao.textContent = "Lados podem formar um triângulo"
        if(ValorA == ValorB && ValorC == ValorB){
            outTipo.textContent = "Tipo: Equilátero"
        }else if(ValorA == ValorB || ValorA == ValorC || ValorC == ValorB){
            outTipo.textContent = "Tipo: Isósceles"
        }else {
            outTipo.textContent = "Tipo: Escaleno"
        }
    } else{
        outSituacao.textContent = "Lados não podem formar um triângulo"
        outTipo.textContent = ""
    }
}



var btExibir = document.getElementById("btExibir")
btExibir.addEventListener("click",calcularFuso)