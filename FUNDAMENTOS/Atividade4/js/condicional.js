function calcularFuso(){
    var inValor = document.getElementById("inValor")
    var outRaizQuadrada = document.getElementById("outRaizQuadrada")

    var numero = Number(inValor.value)

    if(inValor.value == "" || isNaN(numero)){
        alert("Número não informado")
        inValor.focus()
        return
    }
    var raizQuadrada = Math.sqrt(numero)
    if(!Number.isInteger(raizQuadrada)){
        outRaizQuadrada.textContent = "Não há raiz exata para: "+numero.toFixed(0)
        
    }else{
        outRaizQuadrada.textContent = "Raiz Quadrada: "+raizQuadrada.toFixed(2)
    }
}

var btExibir = document.getElementById("btExibir")
btExibir.addEventListener("click",calcularFuso)