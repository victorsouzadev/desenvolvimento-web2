var numeros = []
function addArray(){
    var inNumero = document.getElementById("inNumero")
    var outArray = document.getElementById("outArray")
    
    if(inNumero.value.trim() == ""){
        
        alert("Informe um número válido!")
        inNumero.focus()
        return
    }
    var numero = Number(inNumero.value)
    numeros.push(numero)
    
    outArray.textContent = `Números: ${numeros}`
}
function verificarOrdem(){
    var outSituacao = document.getElementById("outSituacao")
    var auxiliar = numeros.slice()
    auxiliar.sort()
    console.log(auxiliar,numeros)
    if(compararArray(auxiliar,numeros)){
        outSituacao.textContent = "Ok! Números estão em ordem crescente"
        console.log('foi')
    }else{
        outSituacao.textContent = "Atenção... Números não estão em ordem crescente"
    }
    
}

var btAdd = document.getElementById("btAdd")
btAdd.addEventListener("click",addArray)

var btVerificarOrdem = document.getElementById("btVerificarOrdem")
btVerificarOrdem.addEventListener("click",verificarOrdem)

function compararArray(array1,array2){
    var igual = true
    if(array1.length == array2.length){
    for (i in array1){
        if(array1[i]!=array2[i]){
            igual = false
        }
    }
}
return igual
}