const pilotos = ['Vettel','Alonso','Raikkonen','Massa']

pilotos.pop()   //remove ultimo
console.log(pilotos)

pilotos.push('Hamilton')    //Adiciona na ultima posição
console.log(pilotos)

pilotos.shift()     // Remove o primeiro
console.log(pilotos)

pilotos.unshift('Rubinho')   //Adiciona no primeiro
console.log(pilotos)

//Adicionar splice
pilotos.splice(2,0,'Bottas','Massa')
console.log(pilotos)

//Remover splice
pilotos.splice(3,1)
console.log(pilotos)


//Cortar
const algunsPilotos1 = pilotos.slice(2)
console.log(algunsPilotos1)

const algunsPilotos2 = pilotos.slice(1,4)
console.log(algunsPilotos2)