var arrayClubes = []


function addClube(){
    var inNome = document.getElementById("inNome")
    var nome = inNome.value
    
    if( nome.trim() ==""){
        alert("Informe um nome válida!")
        inNome.focus()
        return
    }
    arrayClubes.push(nome)
}

function listarClubes(){
    out = document.getElementById('out')
   
    if(arrayClubes.length == 0){
        alert("Nenhum clube adicionado!")
    } else{
        out.textContent = `${arrayClubes.map((e,i,a) => `${i+1}. ${a[i]}\n`)}`.replace(',','')
        
    }
    
}

function montarTabela(){
    if(arrayClubes.length % 2 == 1){
        alert("Não é possivel combinar")
    }else{
        var auxilar = arrayClubes.slice()
        out.textContent =""
        console.log(auxilar)
        for (var i = 0; i < (arrayClubes.length/2);i++) {
            out.textContent += `${auxilar.pop()} x ${auxilar.shift()}\n`
        }
    }
    

}
var btMostrarTabela = document.getElementById("btMostrarTabela")
btMostrarTabela.addEventListener("click",montarTabela)

var btAdd = document.getElementById("btAdd")
btAdd.addEventListener("click",addClube)

var btListaClubes = document.getElementById("btListaClubes")
btListaClubes.addEventListener("click",listarClubes)