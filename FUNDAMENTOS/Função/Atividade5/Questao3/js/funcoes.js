function resultado(){
    var inName = document.getElementById("inName")
    var out = document.getElementById("out")

    var name = inName.value
    if(inName.value == ""){
        alert("Informar nome")
        inName.focus()
        return
    }


   out.textContent = `Senha inicial: ${gerarSenha(name)}`
   
}   

var btExibir = document.getElementById("btExibir")
btExibir.addEventListener("click",resultado)

function gerarSenha(name){
    name = name.trim()
    return validationName(name) ? getSurname(name)+contarVogais(name) : 'Nome invalido'
}

function validationName(name){
    return name.search(" ") == -1 ? false : true     
}

function getSurname(name){
    name = name.split(" ")
    return name[name.length - 1].toLowerCase()
}
function contarVogais(name){
    out = 0
    var vogais = ['a','e','i','o','u']
    
    for (let index = 0; index < name.length; index++) {
        for (let i = 0; i < vogais.length; i++) {
            if(name.charAt(index)==vogais[i]){
                out++
            }
        }
    }

    return out
}