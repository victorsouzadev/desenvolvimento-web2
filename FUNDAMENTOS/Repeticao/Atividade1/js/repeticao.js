function repeticao(){
    var inValor = document.getElementById("inValor")
    var outTabuada = document.getElementById("outTabuada")

    var numero = Number(inValor.value)

    //Validação da entrada
    if(inValor.value == "" || isNaN(numero)){
        alert("Número não informado")
        inValor.focus()
        return
    }

    tabuada = ""
    for(let i = 1; i<=10; i++){
        tabuada+=`${numero} X ${i} = ${numero*i}\n`
    }
    



    //saída
    outTabuada.textContent = tabuada
}

var btExibir = document.getElementById("btExibir")
btExibir.addEventListener("click",repeticao)