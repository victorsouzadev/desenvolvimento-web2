const alunos = [
    {nome:'Vitor',nota: 7.3, bolsista: false},
    {nome:'Luan',nota: 9.2, bolsista: true},
    {nome:'Pedro',nota: 9.8, bolsista: false},
    {nome:'João',nota: 8.7, bolsista: true}
]

console.log(alunos.map(a=>a.nota))

const resultado = alunos.map(a => a.nota).reduce(function(acumulador,atual){
    console.log(acumulador,atual)
    return acumulador + atual
})

console.log(resultado)