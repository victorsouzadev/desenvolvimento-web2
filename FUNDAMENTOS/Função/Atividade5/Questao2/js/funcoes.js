function natacao(){
    var inName = document.getElementById("inName")
    var inIdade = document.getElementById("inIdade")
    var out = document.getElementById("out")

    var name = inName.value
    var idade = Number(inIdade.value)

    if(inName.value == ""){
        alert("Informar nome")
        inName.focus()
        return
    } else if(inIdade.value == "" || isNaN(idade)|| idade<0){
        alert("Informar idade")
        inIdade.focus()
        return
    }

    out.textContent = `${name}\n${retornarTracos(name)}\nCategoria: ${categorizarAluno(idade)} `
    
}

var btExibir = document.getElementById("btExibir")
btExibir.addEventListener("click",natacao)


function retornarTracos(name){
    out =""
    for (let index = 0; index < name.length; index++) {
        if (name.charAt(index) == " "){
            out += " "
        } else{
            out += "-"
        }
    }
    return out
}
function categorizarAluno(idade){
    return idade>18 ?  "Adulto" : idade<=12 ? "Infatil" : "Juvenil"   
}