function repeticao(){
    var inValor = document.getElementById("inValor")
    var outDecrescentes = document.getElementById("outDecrescentes")

    var numero = Number(inValor.value)
    var input = numero
    //Validação da entrada
    if(inValor.value == "" || isNaN(numero)){
        alert("Número não informado")
        inValor.focus()
        return
    }
    out = ""
    do{
        out += `${numero},`
        numero--
    }while(numero!=0)

    
    //saída
    outDecrescentes.textContent = `Entre ${input} e 1:`+out
}

var btExibir = document.getElementById("btExibir")
btExibir.addEventListener("click",repeticao)