function repeticao(){
    var inValor = document.getElementById("inValor")
    var out = document.getElementById("out")

    var numero = Number(inValor.value)

    //Validação da entrada
    if(inValor.value == "" || isNaN(numero)){
        alert("Número não informado")
        inValor.focus()
        return
    }

    
    //saída
    out.textContent = ""
}

var btExibir = document.getElementById("btExibir")
btExibir.addEventListener("click",repeticao)