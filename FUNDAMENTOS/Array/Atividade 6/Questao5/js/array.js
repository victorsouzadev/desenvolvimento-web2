var candidatos = []

function addCandidato(){
    
    var inNome = document.getElementById("inNome")
    var inAcertos = document.getElementById("inAcertos")
    nome = inNome.value
    acertos = Number(inAcertos.value)
    
    //Validação
    if(acertos <= 0 || isNaN(acertos)){
        alert("Informe uma idade válida!")
        inIdade.focus()
        return
    }
    if( nome.trim() ==""){
        alert("Informe um nome válida!")
        inNome.focus()
        return
    }
    
    candidatos.push({nome:nome,acertos:acertos})
}

function listAll(){
    var out = document.getElementById("out")
    out.textContent = candidatos.map(a => `${a.nome} - ${a.acertos}\n`)
}
function aprovados(){
    var out = document.getElementById("out")
    minimo = prompt("Número de Acertos para Aprovação?")
    out.textContent = candidatos.filter(a=> a.acertos >= minimo).map(a => `${a.nome} - ${a.acertos}\n`)
}

var btAprovados = document.getElementById("btAprovados")
btAprovados.addEventListener("click",aprovados)

var btAdd = document.getElementById("btAdd")
btAdd.addEventListener("click",addCandidato)

var btListAll = document.getElementById("btListAll")
btListAll.addEventListener("click",listAll)