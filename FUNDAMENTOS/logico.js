function compras(trabalho1,trabalho2){
    const compraSorvete = trabalho1 || trabalho2
    const comprarTV50 = trabalho1 && trabalho2
    //const comprarTV32 = !!(trabalho1 ^ trabalho2) //XOR - OU esclusivo
    const comprarTV32 = trabalho1 != trabalho2
    const manterSaudavel = !compraSorvete // Operador unário

    return {sorvete:compraSorvete,comprarTV50,comprarTV32,manterSaudavel}
}



console.log(compras(true,true))
console.log(compras(false,true))
console.log(compras(true,false))
console.log(compras(false,false))