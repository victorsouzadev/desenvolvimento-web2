var erros = []

var sorteado = Math.floor(Math.random()*100) + 1
console.log(sorteado)
const CHANCHES = 6

function apostar(){
    // crie refrencia ao campo de entrada e obetenha seu conteudo...
    var inNumero = document.getElementById("inNumero")
    numero = Number(inNumero.value)

    // valida o numero
    if(numero <= 0 || numero > 100 || isNaN(numero)){
        alert("Informe um número válido!")
        inNumero.focus()
        return
    }

    // referencia as saidas de dados
    var outErros = document.getElementById("outErros")
    var outChances = document.getElementById("outChances")
    var outDica = document.getElementById("outDica")
    //se aposta do jogador for igual ao numero
    if(numero == sorteado){
        alert("Parabéns Você Acertou!!!")
        document.getElementById('btApostar').disabled = true
        document.getElementById('btJogarNovamente').className = 'exibe'
        // troca do estado dos botões
    }else{
        if(erros.indexOf(numero) >= 0){
            alert("Você já apostou o número" + numero + ". Tente outro")
        }else{
            erros.push(numero) // add número ao vetor
            var numErros = erros.length // obtem tamanho do vetor
            var numChanches = CHANCHES - numErros // calcula o numero de chanches
            // exibir numero de erros, conteudo do vetor, numero de chanches disponivel
            outErros.textContent = `Erros ${numErros} (${erros})`
            outChances.textContent = `Chances ${numChanches}`
            if(numChanches == 0){
                alert("Suas Chanches Acabaram...")
                document.getElementById('btApostar').disabled = true
                document.getElementById('btJogarNovamente').className = 'exibe'
            }else{
                
                var dica = numero < sorteado ? "maior" : "menor"
                console.log(dica)
                outDica.textContent =  "Dica: tente um numero " + dica + " que " + numero
            }
        }
    }
    //limpa o campo de entrada e posiciona o cursor neste campo
    inNumero.value = ""
    inNumero.focus()
}

function jogarNovamente(){
    location.reload()
}
var btApostar = document.getElementById("btApostar")
btApostar.addEventListener("click",apostar)
var btJogar = document.getElementById("btJogarNovamente")
btJogar.addEventListener("click",jogarNovamente)
