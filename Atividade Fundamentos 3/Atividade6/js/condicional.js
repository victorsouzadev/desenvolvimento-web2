function calcularFuso(){
    var inValor = document.getElementById("inValor")
    var outNota100 = document.getElementById("outNota100")
    var outNota50 = document.getElementById("outNota50")
    var outNota10 = document.getElementById("outNota10")

    var numero = Number(inValor.value)

    if(inValor.value == "" || isNaN(numero)){
        alert("Número não informado")
        inValor.focus()
        return
    }
    var qt100 = 0
    var qt50 = 0
    var qt10 = 0

    if(numero % 100 != 0){
        qt100 = Math.floor(numero/100)
        numero = numero - qt100*100 
    } 

    if(numero % 50 != 0){
        qt50 = Math.floor(numero/50)
        numero = numero - qt50*50 
    }

    if(numero % 10 != 0){
        alert("Valor inválido para notas disponíveis(R$ 100, 50, 10)")     
    }else{
        qt10 = Math.floor(numero/10)
        numero = numero - qt10*10 
        outNota100.textContent = "Notas de R$ 100: "+qt100
        outNota50.textContent = "Notas de R$ 50: "+qt50
        outNota10.textContent = "Notas de R$ 10: "+qt10
    }
    console.log(numero)

    
    

}

var btExibir = document.getElementById("btExibir")
btExibir.addEventListener("click",calcularFuso)