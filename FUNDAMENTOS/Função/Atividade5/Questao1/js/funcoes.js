function mostrarDados(){
    //input
    var inModelo = document.getElementById("inModelo")
    var inAno = document.getElementById("inAno")
    var inPreco = document.getElementById("inPreco")

    //output 
    var outClassificacao = document.getElementById("outClassificacao")
    var outPrecoVenda = document.getElementById("outPrecoVenda")

    var modelo = inModelo.value
    var ano = Number(inAno.value)
    var preco = Number(inPreco.value)

    
    //Validação da entrada
    if(inModelo.value == ""){
        alert("Informar modelo")
        inModelo.focus()
        return
    } else if(inAno.value == "" || isNaN(ano)|| ano<=0){
        alert("Informar ano")
        inAno.focus()
        return

    } else if(inPreco.value == "" || isNaN(preco)|| preco <= 0){
        alert("Informar preco")
        inPreco.focus()
        return
    }

    var classificacao = classificar(ano)
    preco = calcularVenda(preco,classificacao) 

    outClassificacao.textContent = `${modelo} - ${classificacao}`
    outPrecoVenda.textContent = `Preço venda R$ ${preco.toFixed(2)}` 


}





var btExibir = document.getElementById("btExibir")
btExibir.addEventListener("click",mostrarDados)

function classificar(ano){
    var anoAtual = new Date().getFullYear()
    console.log(anoAtual-ano)
    var diferenca = anoAtual - ano 
    var classificacao = ""
    if(diferenca == 0){
        classificacao = "Novo"
    } else if(diferenca == 1 ||diferenca == 2  ){
        classificacao = "Seminovo"
    }else{
        classificacao = "Usado"
    }
    return classificacao
}


function calcularVenda(valor,status){
     var prVenda = (status == "Novo") ? valor * 1.08 : valor * 1.10
     console.log(prVenda)
     return prVenda
}